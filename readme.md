## Synopsis

A Laravel based social networking web application similar to Facebook.

## Live

[Reseau](http://tranquil-oasis-82129.herokuapp.com)

## Code Example

This web application is made using Laravel(PHP framework),Mysql.It replicates basic functionality of Facebook.  

Bitbucket Repo: [Bitbucket](https://bitbucket.org/karanbali/localspot)

## License

[MIT license](http://opensource.org/licenses/MIT)